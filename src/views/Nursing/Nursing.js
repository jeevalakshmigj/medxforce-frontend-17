
import React, { Component } from "react";
import { HoverableCard } from "../../components/HoverableCard";

import { ServiceCard } from "../../components/ServiceCard";
import { Toast } from "../../components/Toast";
import Breadcrumb from "../../components/BreadCrumb/Breadcrumb";
import './Nursing.css';
import ServiceNursing from "./ServiceNursing";
import List from "./List";

class Nursing extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const path = [
      {
        title: "Dashboard",
        url: "/dashboard"
      },
      {
        title: "buttons",
        url: "/Nursing"
      }
    ];
    return (
      <div className="main-content-container p-4 container-fluid">
        <div className="right-panel">
          <div className="card">
            <div className="card-body">
              <ul className="nav navbar-nav ml-auto" >
                <div className="row">
                <li className="nav-item active list1">
                  <a className="nav-link" href="#" >
                    Services
                  </a>
                </li>
                <li className="nav-item list2">
                  <a className="nav-link" href="#list">
                    List
                  </a>
                </li>
                <li className="nav-item list3">
                  <a className="nav-link" href="#Requests">
                    Requests
                  </a>
                </li>
                </div>
              </ul>
            </div>
          </div>
        </div>
        <div className="row" id="services">
        <ServiceNursing 
        title={"Nursing"}
        count1={10}
        count2={10}
        color1={"flat-color-5"}
        color2={"flat-color-3"}
        />
        <ServiceNursing 
        title={"Home Care"}
        count1={10}
        count2={10}
        color1={"flat-color-2"}
        color2={"flat-color-5"}
        /> 
        <ServiceNursing 
        title={"Nutritional Services"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color1={"flat-color-1"}
        /> 
        </div>
        <div className="row">
        <ServiceNursing 
        title={"Physiotheraphy"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color1={"flat-color-1"}
        />
        <ServiceNursing 
        title={"Speech Theraphy"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color1={"flat-color-1"}
        /> 
        <ServiceNursing 
        title={"Visit Support exe"}
        count1={10}
        count2={10}
        color1={"flat-color-3"}
        color1={"flat-color-1"}
        /> 
        </div>

        <div id = "list">
         <h1>List </h1>

         <List />
        </div>
      </div>
    );
  }
}

export default Nursing;
